# README #

Este README explica como crear/modificar el bot en AWS Lex.

### ¿Para qué es esta sección? ###

* Esta seccioón explica como crear/modificar un bot en AWS Lex usando CloudFormation.
* [Learn Markdown](https://aws.amazon.com/cloudformation/)
* [Learn Markdown](https://aws.amazon.com/es/lex/)

### ¿Qué debo hacer primero? ###

* Instalar la cli de AWS y configurar las credenciales.
* [Learn Markdown](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
* [Learn Markdown](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-quickstart.html)

### ¿Como crear o modificar el bot? ###

* Se debe ejecutar el siguiente comando:
* aws cloudformation deploy --template-file lex_bot.yml --stack-name twilio-bot --capabilities CAPABILITY_NAMED_IAM
* El valor para --stack-name puede ser cambiado a gusto.
* El comando creará un stack en CloudFormation con los recursos definidos en la plantilla.

### Personas involucradas en este proyecto ###

* jose.barrantes@hotmail.es
* https://www.linkedin.com/in/jose-manuel-barrantes-ortiz/