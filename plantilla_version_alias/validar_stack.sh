#!/bin/bash

NOMBRE_STACK_A=twilio-bot-version-alias-A
NOMBRE_STACK_B=twilio-bot-version-alias-B
NOMBRE_STACK_BOT=twilio-bot
MENSAJE_TERMINADO="Stack creado con exito: "
MENSAJE_VERSION_BOT="La nueva version del bot es: "
MENSAJE_ACTUALIZACION_VERSION="Se actualizara el stack $NOMBRE_STACK_BOT con una nueva version para el bot"

echo "Revisando cual de los stack existe"

aws cloudformation describe-stacks --stack-name $NOMBRE_STACK_A --query Stacks[*].StackName --output text

statusA=$(echo $?)

aws cloudformation describe-stacks --stack-name $NOMBRE_STACK_B --query Stacks[*].StackName --output text

statusB=$(echo $?)

echo "Si aparece algún mensaje que contenga (ValidationError) eso significa que alguno de los stacks no existe, no es error"

echo "Qué no panda el cúnico =D"

if test $statusA -eq 0 && test $statusB -eq 0
then
	echo "Ambos stacks serán borrados y se creará el stack $NOMBRE_STACK_A"
	aws cloudformation delete-stack --stack-name $NOMBRE_STACK_A
	aws cloudformation delete-stack --stack-name $NOMBRE_STACK_B
	sleep 2m
	aws cloudformation deploy --template-file lex_version_alias.yml --stack-name $NOMBRE_STACK_A
	echo "$MENSAJE_TERMINADO $NOMBRE_STACK_A"
	versionBot=$(aws cloudformation describe-stacks --stack-name $NOMBRE_STACK_A \
	--query "Stacks[0].Outputs[?OutputKey=='TwilioBotVersion'].OutputValue" --output text)
elif test $statusA -eq 0 && test $statusB -eq 255
then
	echo "Se borrará el stack $NOMBRE_STACK_A y se creará el stack $NOMBRE_STACK_B"
	aws cloudformation delete-stack --stack-name $NOMBRE_STACK_A
    aws cloudformation deploy --template-file lex_version_alias.yml --stack-name $NOMBRE_STACK_B
    echo "$MENSAJE_TERMINADO $NOMBRE_STACK_B"
	versionBot=$(aws cloudformation describe-stacks --stack-name $NOMBRE_STACK_B \
	--query "Stacks[0].Outputs[?OutputKey=='TwilioBotVersion'].OutputValue" --output text)
elif test $statusA -eq 255 && test $statusB -eq 0
then
	echo "Se borrará el stack $NOMBRE_STACK_B y se creará el stack $NOMBRE_STACK_A"
	aws cloudformation delete-stack --stack-name $NOMBRE_STACK_B
    aws cloudformation deploy --template-file lex_version_alias.yml --stack-name $NOMBRE_STACK_A
    echo "$MENSAJE_TERMINADO $NOMBRE_STACK_A"
	versionBot=$(aws cloudformation describe-stacks --stack-name $NOMBRE_STACK_A \
	--query "Stacks[0].Outputs[?OutputKey=='TwilioBotVersion'].OutputValue" --output text)
elif test $statusA -eq 255 && test $statusB -eq 255
then
	echo "Creando stack $NOMBRE_STACK_A"
    aws cloudformation deploy --template-file lex_version_alias.yml --stack-name $NOMBRE_STACK_A
    echo "$MENSAJE_TERMINADO $NOMBRE_STACK_A"
	versionBot=$(aws cloudformation describe-stacks --stack-name $NOMBRE_STACK_A \
	--query "Stacks[0].Outputs[?OutputKey=='TwilioBotVersion'].OutputValue" --output text)
fi

echo "$MENSAJE_VERSION_BOT $versionBot"
echo $MENSAJE_ACTUALIZACION_VERSION
aws cloudformation deploy --template-file ../plantilla_bot/lex_bot.yml \
--stack-name $NOMBRE_STACK_BOT --capabilities CAPABILITY_NAMED_IAM \
--parameter-overrides VersionBot=$versionBot