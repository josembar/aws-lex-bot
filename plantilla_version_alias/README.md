# README #

Este README explica como crear/modificar una nueva versión del bot.

### ¿Para qué es esta sección? ###

* Esta sección explica como crear/modificar una versión del bot bot en AWS Lex usando CloudFormation.
* [Learn Markdown](https://aws.amazon.com/cloudformation/)
* [Learn Markdown](https://aws.amazon.com/es/lex/)

### ¿Qué debo hacer primero? ###

* Instalar la cli de AWS y configurar las credenciales.
* [Learn Markdown](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
* [Learn Markdown](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-quickstart.html)

### ¿Como crear o modificar el la versión del bot? ###

* Este stack debe ser creado habiendo creado el bot primero:
* Se debe ejecutar el siguiente comando estando dentro de esta carpeta:
* ./validar_stack.sh
* El comando creará un stack en CloudFormation con los recursos definidos en la plantilla.

### Personas involucradas en este proyecto ###

* jose.barrantes@hotmail.es
* https://www.linkedin.com/in/jose-manuel-barrantes-ortiz/