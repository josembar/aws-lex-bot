import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

def lambda_handler(event, context):
    logger.debug('event.bot.name={}'.format(event['bot']['name']))
    return dispatch(event)

# funcion para obtener los session attributes
def get_session_attributes(intent_request):
    sessionState = intent_request['sessionState']
    if 'sessionAttributes' in sessionState:
        return sessionState['sessionAttributes']

    return {}

# Funcion para retornar el mensaje.
def close(intent_request, session_attributes, fulfillment_state, message):
    intent_request['sessionState']['intent']['state'] = fulfillment_state
    return {
        'sessionState': {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close'
            },
            'intent': intent_request['sessionState']['intent']
        },
        'messages': [message],
        'sessionId': intent_request['sessionId'],
        'requestAttributes': intent_request['requestAttributes'] if 'requestAttributes' in intent_request else None
    }

# Funcion para responder al 1er mensaje
def darInformacion(intent_request):
    session_attributes = get_session_attributes(intent_request)
    text = "AAAAA. Información del curso. Fechas: Link de grupo de wp: https://chat.whatsapp.com/C0QLSZB3GngJdQU22NhQMx"
    message =  {
            'contentType': 'PlainText',
            'content': text
        }
    #[{'contentType': 'PlainText','content': 'PUT FIRST MESSAGE HERE'},
    #   {'contentType': 'PlainText','content': 'PUT SECOND MESSAGE HERE'}
    #]
    fulfillment_state = "Fulfilled"    
    return close(intent_request, session_attributes, fulfillment_state, message) 

# Funcion que recibe el intent y responde con un mensaje.
def dispatch(intent_request):
    # Cuando se envian mensajes a este bot esta función corre.
    logger.debug('dispatch intent_request={}'.format(intent_request))

    session_attributes = get_session_attributes(intent_request)

    intent_name = intent_request['sessionState']['intent']['name']

    # Se retorna un mensaje dependiento del intent.
    if intent_name == 'Primer_Mensaje':
        return darInformacion(intent_request)

    raise Exception('El intent con nombre ' + intent_name + ' no está soportado')