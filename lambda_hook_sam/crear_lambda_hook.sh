#!/bin/bash

# Se obtiene nombre del bucket para subir el código.
echo "Obteniendo nombre de s3 bucker..."
S3_BUCKET=$(aws cloudformation list-exports --query Exports[*].Value | grep twilio-bot-bucket-archivos | sed 's/"//g' | xargs)
echo "El bucket es: $S3_BUCKET"

#Creación de la lambda.
echo "Se creará/actualizará la lambda hook"
sam deploy -t lambda_hook.yaml \
--stack-name twilio-bot-lambda-hook-sam \
--s3-bucket $S3_BUCKET \
--s3-prefix codigo \
--capabilities CAPABILITY_NAMED_IAM \
--no-confirm-changeset