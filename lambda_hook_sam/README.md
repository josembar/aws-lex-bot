# README #

Este README explica como crear/modificar una función lambda que sera usada como code hook por el bot en AWS Lex.

### ¿Para qué es esta sección? ###

* Esta sección explica como crear/modificar una función lambda usando AWS SAM. Esta lambda servirá como code hook para un bot en AWS Lex.
* [AWS SAM](https://docs.aws.amazon.com/es_es/serverless-application-model/latest/developerguide/what-is-sam.html)
* [AWS Lambda](https://aws.amazon.com/es/lambda/)
* [Attaching a Lambda function to a bot alias](https://docs.aws.amazon.com/lexv2/latest/dg/lambda.html#lambda-attach)
* [Lambda function handler in Python](https://docs.aws.amazon.com/lambda/latest/dg/python-handler.html)

### ¿Qué debo hacer primero? ###

* Instalar la cli de AWS SAM.
* [Instalación de la CLI de AWS SAM](https://docs.aws.amazon.com/es_es/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
* Instalar la cli de AWS y configurar las credenciales.
* [Installing or updating the latest version of the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
* [Quick setup](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-quickstart.html)

### ¿Como crear o modificar la lambda? ###

* Este stack debe ser creado antes de la creación del bot:
* Se debe ejecutar el siguiente comando estando dentro de esta carpeta:
* ./crear_lambda_hook.sh
* El comando creará un stack en CloudFormation con los recursos definidos en la plantilla.

### Personas involucradas en este proyecto ###

* jose.barrantes@hotmail.es
* https://www.linkedin.com/in/jose-manuel-barrantes-ortiz/