# README #

Este README explica como crear/modificar un s3 bucket en AWS.

### ¿Para qué es esta sección? ###

* Esta sección explica como crear/modificar una función lambda usando CloudFormation. Esta lambda servirá como code hook para un bot en AWS Lex .
* [AWS CloudFormation](https://aws.amazon.com/cloudformation/)
* [AWS S3](https://aws.amazon.com/es/s3/)

### ¿Qué debo hacer primero? ###

* Instalar la cli de AWS y configurar las credenciales.
* [Installing or updating the latest version of the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
* [Quick setup](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-quickstart.html)

### ¿Como crear o modificar el bot? ###

* Este stack debe ser creado antes de la creación de la lambda para code hook:
* Se debe ejecutar el siguiente comando:
* aws cloudformation deploy --template-file bucket_archivos.yml --stack-name twilio-bot-bucket-archivos
* El valor para --stack-name puede ser cambiado a gusto.
* El comando creará un stack en CloudFormation con los recursos definidos en la plantilla.

### Personas involucradas en este proyecto ###

* jose.barrantes@hotmail.es
* https://www.linkedin.com/in/jose-manuel-barrantes-ortiz/